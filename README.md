certChecker services
===

Provides elementary, REST-compliant services for monitoring the validity of remote SSL/TLS certificates. Only SSL and TLS tunnels are currently supported (i.e. STARTTLS is not yet supported).

The target addresses are defined in the configuration file (see below).


> **Usage:**  Call the *app* application defined in wsgi.py from a WSGI-compliant server. For example using *gunicorn*: `$ gunicorn wsgi:app`  
>
> Resources are called using GET requests:  `http://url/<function>/<hostkey>`


##Available resources

- `validity`  
  Checks certificate expiration date. Returns remaining days until expiration or 0 if expired.

- `verify`  
  Checks wether the certificate subject matches the server's name/fqdn; return OK or KO. Wildcards are not currently supported

- `show`  
  Returns certificate content


##Configuration file

Settings are stored in the *certChecker.conf* file, located in the local directory, with the following format. All configuration sections are mandatory, additional settings will be silently ignored.

```
## certChecker.conf

[hosts]
hostkey1 = fqdn.of.tls.service1
hostkey2 = fqdn.of.tls.service2
# etc...

[auth]
username = alice
password =  mysecret  # hashed password as return by sha256_crypt.encrypt()
```