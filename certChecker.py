# -*- coding: utf-8 -*-

"""
	certChecker
	~~~~~~~~~~~
	The CertChecker class provides the methods called by the web services.

	:copyright: (c) 2015 by Nicolas Perraud
	:licence: CeCILL version 2.1, see LICENCE for more details
"""

import socket
import ssl
import flask
import configparser
from functools import wraps
from passlib.hash import sha256_crypt
from datetime import datetime


class CertChecker:

	def __init__(self, configfile = 'certChecker.conf'):
		self.err = None
		self.configfile = configfile
		self.config = None


	def check_cert_validity(self, hostkey):
		"""Check remote certificate expiration date

		Args:
			hostkey: index key of the server address as defined in global
				configuration

		Returns:
			number of days until expiration (string), 0 if expired
		"""
		self.err = None

		if not self.config:
			self.__get_config()

		host = self.__get_host_by_key(hostkey)
		if not host:
			flask.abort(400)	# abort() will never return

		cert = self.__query_certificate(host)
		if not cert:
			flask.abort(500)

		days = self.__check_expiration_date(cert)
		if not days:
			flask.abort(500)

		return '%s\n' % str(days)


	def verify_cert_subject(self, hostkey):
		"""Check if remote certificate subject matches server address

		Args:
			hostkey: index key for the server address as defined in global
				configuration

		Returns:
			'OK' if certificate subject matches server address
			'KO' otherwise
			Call flask.abort() if error occured
		"""
		self.err = None

		if not self.config:
			self.__get_config()

		host = self.__get_host_by_key(hostkey)
		if not host:
			flask.abort(400)

		cert = self.__query_certificate(host)
		if not cert:
			flask.abort(500)

		if 'subject' not in cert:
			self.err = 'Unknown certificate format'
			flask.abort(500)

		commonName = None
		for field in cert['subject']:
			if field[0][0] == "commonName":
				commonName = field[0][1]
				break

		if not commonName:
			self.err = 'Unknown certificate format'
			flask.abort(500)

		if host != commonName:
			return "KO|Certificate subject does not match." + "\n"

		return "OK|All good" + "\n"


	def show_cert(self, hostkey):
		"""Print out certificate content

		Args:
			hostkey: index key for the server address as defined in global
				configuration

		Returns:
			Certificate content
		"""
		self.err = None

		if self.config is None:
			self.__get_config()

		host = self.__get_host_by_key(hostkey)
		if not host:
			flask.abort(400)

		cert = self.__query_certificate(host)
		if not cert:
			flask.abort(500)

		return str(cert)


	def __get_config(self):
		try:
			self.config = configparser.ConfigParser()
			self.config.read(self.configfile)
		except:
			self.err = 'Configuration file not found'
			flask.abort(500)

		try:
			self.config['hosts']
		except KeyError:
			self.err = 'Configuration file has an unknown format'
			flask.abort(500)


	def __query_certificate(self, host, port = 443):
		"""Fetch remote certificate.

		Args:
		 	host: host address (IP address or fqdn)
			port: SSL service network port (TCP)

		Returns:
			Certificate as dictionnary
			None if error occured, self.err is update
		"""
		cert = None

		try:
			context = ssl.SSLContext(ssl.PROTOCOL_SSLv23)
			context.verify_mode = ssl.CERT_OPTIONAL
			ssl_sock = context.wrap_socket(socket.socket(socket.AF_INET, socket.SOCK_STREAM))
			ssl_sock.settimeout(5)
			ssl_sock.connect((host, port))
			cert = ssl_sock.getpeercert()
			ssl_sock.close()
		except socket.timeout:
			self.err = 'Failed to connect to SSL server'
			return None
		except socket.gaierror:
			self.err = 'Failed to resolve host name'
			return None

		return cert


	def __get_host_by_key(self, hostkey):
		host = self.config['hosts'].get(hostkey, None)
		if not host:
			self.err = 'Host key not registered\n'

		return host


	def __check_expiration_date(self, cert):
		"""Returns remaining number of days before cert expiration date.

		Args:
			cert: certificate as dictionnary

		Returns:
			Number of day remaining until expiration, 0 if expired
			None if error occured
		"""

		if 'notAfter' not in cert:
			self.err = 'notAfter field not found in certificate'
			return None

		try:
			expire_date = datetime.strptime(cert['notAfter'], "%b %d %H:%M:%S %Y %Z")
			expire_in = expire_date - datetime.now()

			if expire_in.days < 0:
				return 0
			else:
				return expire_in.days
		except:
			pass

		self.err = 'Invalid certificate'
		return None


	def requires_auth(self, f):
		@wraps(f)
		def decorated(*args, **kwargs):
			auth = flask.request.authorization
			if not auth:
				flask.abort(401)

			self.__get_config()
			pass_ok = False
			if auth.username == self.config['auth'].get('username', False):
				try:
					pass_ok = sha256_crypt.verify(auth.password,
								self.config['auth'].get('password', False))
				except:
					pass
			if pass_ok is False:
				flask.abort(401)

			return f(*args, **kwargs)

		return decorated
