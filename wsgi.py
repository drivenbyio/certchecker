# -*- coding: utf-8 -*-

"""
	certChecker.wsgi
	~~~~~~~~~~~

	:copyright: (c) 2015 by Nicolas Perraud.
	:licence: CeCILL version 2.1, see LICENCE for more details.
"""

from flask import Flask
from certChecker import CertChecker

app = Flask(__name__)

Checker = CertChecker('certChecker.conf')

@app.route('/')
def index():
	return "You lost?", 200

@app.route('/validity/<hostkey>', methods=['GET'])
@Checker.requires_auth
def validity(hostkey):
	return Checker.check_cert_validity(hostkey), 200

@app.route('/verify/<hostkey>', methods=['GET'])
@Checker.requires_auth
def verify(hostkey):
	return Checker.verify_cert_subject(hostkey), 200

@app.route('/show/<hostkey>', methods=['GET'])
@Checker.requires_auth
def show(hostkey):
	return Checker.show_cert(hostkey), 200

#
# Error handlers
#
@app.errorhandler(500)
def server_error(error):
	return '%s\n%s' % (error, Checker.err), error.code

@app.errorhandler(400)
def server_error(error):
	return '%s\n%s' % (error, Checker.err), error.code

@app.errorhandler(401)
def server_error(error):
	return 'Authentication failed', 401

@app.errorhandler(404)
def server_error(error):
	return '%s: Resource not found' % str(error.code), error.code

# testing only
if __name__ == '__main__':
    app.run(host='localhost', port=8888, debug=True)
